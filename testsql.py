from entity import Food,Donar,Animal,Login,db
import sqlalchemy
from sqlalchemy import create_engine,inspect
from sqlalchemy.sql import text
db.drop_all(bind='__all__')
db.create_all()
from datetime import datetime,date
dater=date.today()
engine = create_engine('sqlite:///databases/strayservice.db')
inspector = inspect(engine)
print(" ")

print("LOGIN")
print(" ")
with engine.connect() as con:

    data = ({ "email": "raj@gmail.com", "password": "123", "name": "Raj Sharma","location":"Howrah","usertype":"r" },
            { "email": "simran@gmail.com", "password": "123", "name": "Simran Sharma","location":"Jadavpur","usertype":"r" },
            { "email": "club@gmail.com", "password": "123", "name": "Club","location":"Jadavpur","usertype":"c" },
            { "email": "animallover@gmail.com", "password": "123", "name": "Animal Lover","location":"Salt Lake","usertype":"a" },
            { "email": "resident@gmail.com", "password": "123", "name": "Resident","location":"Behala","usertype":"r" },
            
            

            )

    statement = text("""INSERT INTO login(email, password, name,location,usertype) VALUES(:email, :password, :name,:location,:usertype)""")

    for line in data:
        con.execute(statement, **line)



with engine.connect() as con:

     rs = con.execute('SELECT * FROM Login')

     for row in rs:
         print (row)

print(" ")
with engine.connect() as con:

    rs = con.execute('SELECT * FROM Login WHERE location="Jadavpur" ')

    for row in rs:
        print (row)

#  NEXT BEGINS HERE
print(" ")

print("ANIMAL")
print(" ")


with engine.connect() as con:

    data = ( { "datetime":datetime.utcnow(),"image_file": "Injured.jpg","animal":"cat", "location": "Kalighat", "description": "Hit by car.","bleeding":"yes","immediate":"yes","longterm":"yes" },
            { "datetime":datetime.utcnow(),"image_file": "Abused.jpg","animal":"dog", "location": "Kolaghat", "description": "Hit by Auto.","bleeding":"yes","immediate":"yes","longterm":"no" },
            { "datetime":datetime.utcnow(),"image_file": "index.png","animal":"cow", "location": "Kolaghat", "description": "Hit by Truck.","bleeding":"yes","immediate":"yes","longterm":"yes" },
            )

    statement = text("""INSERT INTO Animal(datetime,image_file, animal,location,description, bleeding, immediate, longterm) VALUES(:datetime,:image_file,:animal ,:location,:description, :bleeding,:immediate, :longterm)""")

    for line in data:
        con.execute(statement, **line)




with engine.connect() as con:

    rs = con.execute('SELECT * FROM Animal')

    for row in rs:
        print (row)


print(" ")
with engine.connect() as con:

    rs = con.execute('SELECT * FROM Animal WHERE location="Kolaghat" ')

    for row in rs:
        print (row)
print(" ")

with engine.connect() as con:
    data = ( { "datetime":datetime.utcnow()})
    rs = con.execute('SELECT * FROM Animal WHERE datetime BETWEEN "2020-05-07" AND "2020-05-09"')
    
    for row in rs:
        print (row)
print(" ")


# DONAR BEGINS
print("DONAR")

with engine.connect() as con:

    data1 = ( { "pick_up_date": "Monday", "pick_up_time": "Noon", "name": "Bisri Mandal","location":"DL-134"},
            { "pick_up_date": "Monday", "pick_up_time": "Noon", "name": "Sirsa Munda","location":"CL-100"},
            {"pick_up_date": "Friday", "pick_up_time": "Morning", "name": "Hari Lodha","location":"KL-200"},
            )

    statement1 = text("""INSERT INTO Donar( pick_up_date, pick_up_time, name, location) VALUES(:pick_up_date, :pick_up_time, :name,:location)""")

    for line in data1:
        con.execute(statement1, **line)




with engine.connect() as con:

    rs = con.execute('SELECT * FROM Donar')

    for row in rs:
        print (row)


print(" ")

# Food begins

with engine.connect() as con:

    data1 = ( { "food_type": "Chicken", "food_quantity_value": "2", "food_quantity_metric": "Kg"},
            { "food_type": "Mutton", "food_quantity_value": "5", "food_quantity_metric": "Kg"},
            { "food_type": "Milk", "food_quantity_value": "7", "food_quantity_metric": "Liters"},
            )

    statement1 = text("""INSERT INTO Food( food_type, food_quantity_value, food_quantity_metric) VALUES(:food_type, :food_quantity_value, :food_quantity_metric)""")

    for line in data1:
        con.execute(statement1, **line)


print("FOOD")

with engine.connect() as con:

    rs = con.execute('SELECT * FROM Food')

    for row in rs:
        print (row)

print(" ")
print("FOOD DONAR DETAILS ")

with engine.connect() as con:

    rs = con.execute('SELECT * FROM Donar NATURAL JOIN Food WHERE pick_up_date="Monday" AND pick_up_time="Noon" ')

    for row in rs:
        print (row)
