import unittest
import os
from io import BytesIO
from entity import Food,Donar,Animal,Login,db
from control import app
from PIL import Image
from datetime import date,timedelta
from bs4 import BeautifulSoup


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        db.create_all()
        self.app = app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all(bind='__all__')
        db.drop_all()

    
    

class TestLogin(TestCase):

    def login(self, email, password):
        return self.app.post('/login',data=dict(email=email, password=password),follow_redirects=True)

    def test_residentlogin_successful(self):
        login = Login(email='rahul@example.com',password='newpass123',usertype='r',name='Rahul Bose',location='Salt Lake')
        db.session.add(login)
        db.session.commit()
        response = self.login('rahul@example.com', 'newpass123')
        self.assertIn(b'You have been logged in!', response.data)
        #print (response.get_data(as_text=True))

    def test_residentlogin_unsuccessful(self):
        response = self.login('rahul@example.com', 'newpass123')
        self.assertIn(b'Login Unsuccessful. Please check username and password!', response.data)
    
 
class TestFood(TestCase):

    def viewfoodreport(self, pick_up_date,pick_up_time):
        return self.app.post('/ViewFoodReport',
        data=dict(pick_up_date=pick_up_date,pick_up_time=pick_up_time),follow_redirects=True)

    def viewfooddetails(self, pick_up_date,pick_up_time):
        return self.app.post('/ViewFoodDetails?date='+pick_up_date+'&time='+pick_up_time,follow_redirects=True)


    def pledgefoodpublic(self,name,location,pick_up_date,pick_up_time,food_type,food_quantity_metric,food_quantity_value):
        return self.app.post('/PublicFoodDonation',
        data=dict(name=name,location=location,pick_up_date=pick_up_date,pick_up_time=pick_up_time,food_type=food_type,food_quantity_metric=food_quantity_metric,food_quantity_value=food_quantity_value),follow_redirects=True)
    
    def pledgefoodresident(self,name,location,pick_up_date,pick_up_time,food_type,food_quantity_metric,food_quantity_value):
        return self.app.post('/ResidentFoodDonation?location='+location+'&name='+name,
        data=dict(name=name,location=location,pick_up_date=pick_up_date,pick_up_time=pick_up_time,food_type=food_type,food_quantity_metric=food_quantity_metric,food_quantity_value=food_quantity_value),follow_redirects=True)

   

    def test_correct_public_pledgedonation(self):
        response = self.pledgefoodpublic(name='Sagnik Banerjee',location='Salt Lake',pick_up_date='Monday',pick_up_time='Morning',food_type='Chicken', food_quantity_metric='Kg', food_quantity_value=5)
        self.assertIn(b'Thank You for Donating Food', response.data)
        donar = Donar.query.filter_by(name='Sagnik Banerjee',location='Salt Lake').first()
        food = Food.query.filter_by(food_type='Chicken', food_quantity_metric='Kg', food_quantity_value=5).first()
        self.assertEqual(food.donar_id,donar.donar_id)

    def test_incorrect_public_pledgedonation(self):    
        response = self.pledgefoodpublic(name='S'*800,location='Salt Lake',pick_up_date='Monday',pick_up_time='Morning',food_type='Chicken', food_quantity_metric='Kg', food_quantity_value=5)
        self.assertIn(b'Field must be between 1 and 512 characters long.', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.find("input", {"class": "form-control form-control-lg is-invalid"})
        self.assertTrue(len(t['value'])>512)

    
    def test_resident_pledgedonation(self):
        response = self.pledgefoodresident(name='Sagnik Banerjee',location='Behala',pick_up_date='Monday',pick_up_time='Morning',food_type='Chicken', food_quantity_metric='Kg', food_quantity_value=5)
        self.assertIn(b'Thank You for Donating Food', response.data)
        donar = Donar.query.filter_by(name='Sagnik Banerjee',location='Behala').first()
        food = Food.query.filter_by(food_type='Chicken', food_quantity_metric='Kg', food_quantity_value=5).first()
        self.assertEqual(food.donar_id,donar.donar_id)

    def test_view_foodreport(self):
        response =self.pledgefoodpublic(name='Sagnik Banerjee',location='Behala',pick_up_date='Monday',pick_up_time='Morning',food_type='Chicken', food_quantity_metric='Kg', food_quantity_value=5)
        response =self.pledgefoodpublic(name='Sahil Roy',location='Salt Lake',pick_up_date='Monday',pick_up_time='Morning',food_type='Mutton', food_quantity_metric='Kg', food_quantity_value=9)
        response =self.pledgefoodpublic(name='Sachin Agarwal',location='Behala',pick_up_date='Friday',pick_up_time='Noon',food_type='Chicken', food_quantity_metric='Kg', food_quantity_value=3)
        response =self.viewfoodreport(pick_up_date='Monday',pick_up_time='Morning')
        self.assertIn(b'Found Records', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.findAll("tr")
        r=Donar.query.join(Food).filter(Donar.pick_up_date=='Monday',Donar.pick_up_time=='Morning').all()
        self.assertEqual(len(t)-1,len(r))

        response =self.viewfoodreport(pick_up_date='Tuesday',pick_up_time='Noon')
        self.assertIn(b'No Records Found', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.findAll("tr")
        r=Donar.query.join(Food).filter(Donar.pick_up_date=='Tuesday',Donar.pick_up_time=='Noon').all()
        self.assertEqual(len(t)-1,len(r))
    
    def test_view_fooddetails(self):
        self.test_resident_pledgedonation()
        response =self.viewfooddetails(pick_up_date='Monday',pick_up_time='Morning')
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.findAll("td")
        r=db.session.query(Food,Donar).filter(Donar.pick_up_date=='Monday',Donar.pick_up_time=='Morning').join(Food,Food.donar_id==Donar.donar_id).first()
        self.assertEqual(r[1].name,t[0].text)
        self.assertEqual(r[1].location,t[1].text)
        self.assertEqual(r[0].food_type,t[2].text) 
        self.assertEqual(r[0].food_quantity_value,int(t[3].text))
        self.assertEqual(r[0].food_quantity_metric,t[4].text)
        #print (response.get_data(as_text=True))
        

class TestAnimal(TestCase):

    def searchforadoption(self,location,animal):
        return self.app.post('/SearchForAdoption',data=dict(location=location,animal=animal),follow_redirects=True)
    
    def viewabusereport(self,start_date,end_date,animal):
        return self.app.post('/ViewAbuseReport',data=dict(start_date=start_date,end_date=end_date,animal=animal),follow_redirects=True)


    def reportanimalabuse(self,image_file,animal,location, description, bleeding, immediate, longterm):
        i = Image.open(image_file)
        imgByteArr = BytesIO()
        i.save(imgByteArr, format='jpeg')
        i = imgByteArr.getvalue()
        return self.app.post('/ReportAnimalAbuse',
        data=dict(image_file=image_file,picture=(BytesIO(i), 'default.jpg'),animal=animal,location=location, description=description, bleeding=bleeding, immediate=immediate,longterm=longterm),follow_redirects=True,content_type='multipart/form-data')
    
    def showanimal(self,name):
        return self.app.post('/AnimalDetails?name='+name)
    
    def test_report_animalabuse(self):
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Behala', description='Malnourished', bleeding='yes', immediate='no', longterm='yes')
        self.assertIn(b'Your Abuse Report has been submitted! We will take the action asap', response.data)
    
    def test_incorrectreport_animalabuse(self):
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Salt Lake', description='M'*530, bleeding='yes', immediate='no', longterm='yes')
        self.assertIn(b'Field must be between 1 and 512 characters long.', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.find("input", {"class": "form-control form-control-lg is-invalid"})
        self.assertTrue(len(t['value'])>512)

        response = self.reportanimalabuse(image_file='static/tests/try.jpg',animal='Dog',location='Behala', description='Malnourished', bleeding='yes', immediate='no', longterm='yes')
        self.assertIn(b'Picture should be less than 6mb!',response.data)

    def test_incorrect_searchforadoption(self):
        response = self.searchforadoption(location='Alipore',animal='Dog')
        self.assertIn(b'No Records Found', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.findAll("tr")
        r=Animal.query.filter_by(location='Alipore').all()
        self.assertEqual(len(t)-1,len(r))
    


    def test_correct_searchforadoption(self):
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Behala', description='Malnourished', bleeding='yes', immediate='no', longterm='yes')
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Behala', description='Bleeding', bleeding='yes', immediate='no', longterm='no')
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Salt Lake', description='Bruises', bleeding='yes', immediate='no', longterm='no')
        response = self.searchforadoption(location='Behala',animal='Dog')
        self.assertIn(b'Click on images to view details', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.findAll("tr")
        r=Animal.query.filter_by(location='behala').all()
        self.assertEqual(len(t)-1,len(r))
        
    def test_incorrect_view_animalreport(self):
        response = self.viewabusereport(start_date='',end_date='',animal='Dog')
        self.assertIn(b'Not a valid date value', response.data)
        
        response = self.viewabusereport(start_date='2020-04-22',end_date='2020-04-22',animal='Dog')
        self.assertIn(b'No Records Found', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.findAll("tr")
        r=Animal.query.filter(Animal.datetime>='2020-04-22',Animal.datetime<='2020-04-22').all()
        self.assertEqual(len(t)-1,len(r))
    
    def test_correct_view_animalreport(self):
        t=date.today()
        n=t+timedelta(1)
        t=t.strftime('%Y-%m-%d')
        n=n.strftime('%Y-%m-%d')
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Behala', description='Malnourished', bleeding='yes', immediate='no', longterm='yes')
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Behala', description='Bleeding', bleeding='yes', immediate='no', longterm='no')
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Salt Lake', description='Bruises', bleeding='yes', immediate='no', longterm='no')
        response = self.viewabusereport(start_date=t,end_date=t,animal='Dog')
        self.assertIn(b'Click on images to view details', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        tt = soup.findAll("tr")
        r=Animal.query.filter(Animal.datetime>=t,Animal.datetime<=n).all()
        self.assertEqual(len(tt)-1,len(r))
    
    
    def test_showanimal(self):
        response = self.reportanimalabuse(image_file='static/tests/default.jpg',animal='Dog',location='Behala', description='Malnourished and Beaten', bleeding='yes', immediate='no', longterm='yes')
        animal = Animal.query.filter_by(location='behala').first()
        response = self.showanimal(name=animal.image_file)
        self.assertIn(b'Animal Details', response.data)
        soup = BeautifulSoup(response.get_data(as_text=True), 'html.parser')
        t = soup.findAll("td")
        self.assertEqual(t[1].text.strip(),animal.location)
        self.assertEqual(t[3].text.strip(),animal.description)
        self.assertEqual(t[5].text.strip(),animal.bleeding)
        self.assertEqual(t[7].text.strip(),animal.immediate)
        self.assertEqual(t[9].text.strip(),animal.longterm)

  
       

        
    
        
    


if __name__ == '__main__':
    unittest.main()