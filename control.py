import os
from datetime import date,datetime,timedelta
import secrets
from PIL import Image
from flask import Flask, render_template, url_for,redirect,flash,request,session,abort
from boundary import LoginForm, AnimalForm, PledgePublicForm, PledgeResidentForm, ViewFoodForm, SearchAnimalForm, DateForm
from entity import Login, Animal, Donar, Food
from flask_sqlalchemy import SQLAlchemy
from werkzeug.exceptions import RequestEntityTooLarge
from gevent.pywsgi import WSGIServer

app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'
app.config['MAX_CONTENT_LENGTH'] =6*1024*1024
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///databases/strayservice.db'



@app.route("/large")
def large():
    flash('Picture should be less than 6mb!','danger')
    flash('TRY AGAIN!','danger')
    return render_template('413.html')

@app.errorhandler(RequestEntityTooLarge)
def page_not_found(e):
    session.pop('_flashes', None)
    return redirect(url_for('large'))
    

db = SQLAlchemy(app)


def save_picture(form_picture):
    random_hex = secrets.token_hex(4)
    picture_fn = random_hex + form_picture.filename
    output_size = (512, 512)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    picture_path = os.path.join(app.root_path, 'static/animal', picture_fn)
    i.save(picture_path)
    return picture_fn


@app.route("/")
def home():
    return render_template('home.html')


 #ANIMAL MANAGER

@app.route("/ReportAnimalAbuse", methods=['GET', 'POST'])
def ReportAnimalAbuse():
    flash('Upload Picture of size less than 6 mb', 'warning')
    form=AnimalForm()
    if form.validate_on_submit():
        session.pop('_flashes', None)
        image_file = save_picture(form.picture.data)
        animal = Animal(image_file=image_file,location=form.location.data.lower(),animal=form.animal.data,description=form.description.data, bleeding=form.bleeding.data, immediate=form.immediate.data, longterm=form.longterm.data)
        db.session.add(animal)
        db.session.commit()
        flash('Your Abuse Report has been submitted! We will take the action asap', 'success')
        return redirect(url_for('AnimalDetails',name=image_file))
    return render_template('raa.html', title='Report Animal Abuse',form=form)



@app.route("/animalloverlogin", methods=['GET', 'POST'])
def animalloverlogin():
    form = LoginForm()
    if form.validate_on_submit():
        username = Login.query.filter_by(email=form.email.data).first()
        if username and username.password == form.password.data and username.usertype=='a':
            flash('You have been logged in!', 'success')
            flash('Welcome '+str(username.name)+"!", 'success')
            return redirect(url_for('SearchForAdoption'))
        else:
            flash('Login Unsuccessful. Please check username and password!', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route("/SearchForAdoption", methods=['GET', 'POST'])
def SearchForAdoption():
    form=SearchAnimalForm()
    if form.validate_on_submit():
        return redirect(url_for('ShowAnimals',location=form.location.data.lower(),animal=form.animal.data))
    return render_template('sfa.html', title='Search For Adoption',form=form)


@app.route("/ShowAnimal")
def ShowAnimal():
    page = request.args.get('page', 1, type=int)
    a=request.args.get('animal')
    y1=request.args.get('y1')
    m1=request.args.get('m1')
    d1=request.args.get('d1')
    y2=request.args.get('y2')
    m2=request.args.get('m2')
    d2=request.args.get('d2')
    start = date(year=int(y1),month=int(m1),day=int(d1))
    end = date(year=int(y2),month=int(m2),day=int(d2))+timedelta(days=1)
    
    if a=="All":
        t=Animal.query.filter(Animal.datetime>=start,Animal.datetime<=end).first()
        animal=Animal.query.filter(Animal.datetime>=start,Animal.datetime<=end).paginate(page=page, per_page=30)
    else:
        t=Animal.query.filter(Animal.datetime>=start,Animal.datetime<=end,Animal.animal==a).first()
        animal=Animal.query.filter(Animal.datetime>=start,Animal.datetime<=end,Animal.animal==a).paginate(page=page, per_page=30)

    if t:
        flash('Click on images to view details', 'success')
    else:
        flash('No Records Found', 'danger')
    return render_template('sa2.html', title='Animals for Adoption',posts=animal,y1=y1,m1=m1,d1=d1,y2=y2,m2=m2,d2=d2)




@app.route("/ShowAnimals")
def ShowAnimals():
    page = request.args.get('page', 1, type=int)
    location=request.args.get('location')
    a=request.args.get('animal')
    if a=="All":
        t=Animal.query.filter_by(location=location).first()
        animal = Animal.query.filter_by(location=location).paginate(page=page, per_page=30)
    else:
        t=Animal.query.filter_by(location=location).filter_by(animal=a).first()
        animal = Animal.query.filter_by(location=location).filter_by(animal=a).paginate(page=page, per_page=30)

    if t:
        flash('Click on images to view details', 'success')
    else:
        flash('No Records Found', 'danger')
    return render_template('sa.html', title='Animals for Adoption',posts=animal,location=location)

@app.route("/AnimalDetails", methods=['GET', 'POST'])
def AnimalDetails():
    name=request.args.get('name')
    animal = Animal.query.filter_by(image_file=name).first()
    return render_template('ad.html', title='Animal Details',image_name=name,post=animal)




@app.route("/animallogin", methods=['GET', 'POST'])
def animallogin():
    form = LoginForm()
    if form.validate_on_submit():
        username = Login.query.filter_by(email=form.email.data).first()
        if username and username.password == form.password.data and username.usertype=='c':
            flash('You have been logged in!', 'success')
            flash('Welcome '+str(username.name)+"!", 'success')
            return redirect(url_for('ViewAbuseReport'))
        else:
            flash('Login Unsuccessful. Please check username and password!', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route("/ViewAbuseReport", methods=['GET', 'POST'])
def ViewAbuseReport():
    form=DateForm()
    if form.validate_on_submit():
        y1=form.start_date.data.strftime('%Y')
        m1=form.start_date.data.strftime('%m')
        d1=form.start_date.data.strftime('%d')
        y2=form.end_date.data.strftime('%Y')
        m2=form.end_date.data.strftime('%m')
        d2=form.end_date.data.strftime('%d')
        return redirect(url_for('ShowAnimal',y1=y1,m1=m1,d1=d1,y2=y2,m2=m2,d2=d2,animal=form.animal.data))
    return render_template('var.html', title='View Abuse Report',form=form)



#LOGIN MANAGER
@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        username = Login.query.filter_by(email=form.email.data).first()
        if username and username.password == form.password.data and username.usertype=='r':
            flash('You have been logged in!', 'success')
            flash('Welcome '+str(username.name)+"!", 'success')
            return redirect(url_for('ResidentFoodDonation',name=username.name,location=username.location))
        else:
            flash('Login Unsuccessful. Please check username and password!', 'danger')
    return render_template('login.html', title='Login', form=form)


#FOOD MANAGER

@app.route("/PledgeFoodDonation")
def PledgeFoodDonation():
    return render_template('fpd.html', title='Pledge Food Donation')


@app.route("/PublicFoodDonation",methods=['GET', 'POST'])
def PublicFoodDonation():
    form = PledgePublicForm()
    if form.validate_on_submit():
        donar=Donar(name=form.name.data,location=form.location.data,pick_up_date=form.pick_up_date.data,pick_up_time=form.pick_up_time.data)
        food=Food(food_type=form.food_type.data, food_quantity_metric=form.food_quantity_metric.data, food_quantity_value=form.food_quantity_value.data)
        db.session.add(donar)
        db.session.add(food)
        db.session.commit()
        flash('Thank You for Donating Food', 'success')
        return redirect(url_for('FoodDetails',id=donar.donar_id))
    return render_template('pd.html', title='Public Pledge Donation',form=form)

@app.route("/ResidentFoodDonation",methods=['GET', 'POST'])
def ResidentFoodDonation():
    form = PledgeResidentForm()
    name=request.args.get('name')
    location=request.args.get('location')
    if form.validate_on_submit():
        donar=Donar(name=name,location=location,pick_up_date=form.pick_up_date.data,pick_up_time=form.pick_up_time.data)
        food=Food(food_type=form.food_type.data, food_quantity_metric=form.food_quantity_metric.data, food_quantity_value=form.food_quantity_value.data)
        db.session.add(donar)
        db.session.add(food)
        db.session.commit()
        flash('Thank You for Donating Food', 'success')
        return redirect(url_for('FoodDetails',id=donar.donar_id))
    return render_template('rpd.html', title='Resident Pledge Donation',form=form)



@app.route("/FoodDetails", methods=['GET', 'POST'])
def FoodDetails():
    id=request.args.get('id')
    donar = Donar.query.filter_by(donar_id=id).first()
    food = Food.query.filter_by(donar_id=id).first()
    return render_template('fr.html', title='Food Details',donar=donar,food=food)
    


@app.route("/foodlogin", methods=['GET', 'POST'])
def foodlogin():
    form = LoginForm()
    if form.validate_on_submit():
        username = Login.query.filter_by(email=form.email.data).first()
        if username and username.password == form.password.data and username.usertype=='c':
            flash('You have been logged in!', 'success')
            flash('Welcome '+str(username.name)+"!", 'success')
            return redirect(url_for('ViewFoodReport'))
        else:
            flash('Login Unsuccessful. Please check username and password!', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route("/ViewFoodReport", methods=['GET', 'POST'])
def ViewFoodReport():
    form=ViewFoodForm()
    if form.validate_on_submit():
        date=form.pick_up_date.data
        time=form.pick_up_time.data
        return redirect(url_for('ViewFoodDetails',date=date,time=time))
    return render_template('vfr.html', title='View Food Report',form=form)

@app.route("/ViewFoodDetails", methods=['GET', 'POST'])
def ViewFoodDetails():
    date=request.args.get('date')
    time=request.args.get('time')
    page = request.args.get('page', 1, type=int)
    t = Donar.query.filter_by(pick_up_date=date,pick_up_time=time).all()
    donar=db.session.query(Food,Donar).filter(Donar.pick_up_date==date,Donar.pick_up_time==time).join(Food,Food.donar_id==Donar.donar_id).paginate(page=page, per_page=50)
    if t:
        flash('Found Records', 'success')
    else:
        flash('No Records Found', 'danger')
    return render_template('tfr.html', title='Food Details',posts=donar,date=date,time=time)
    
    


if __name__ == '__main__':
    http_server = WSGIServer(('0.0.0.0', 5000), app)
    http_server.serve_forever()
    #app.run()
