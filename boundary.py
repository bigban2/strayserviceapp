from datetime import datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField,SelectField, FloatField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length, Email, EqualTo, InputRequired
from flask_wtf.file import FileField, FileAllowed


class LoginForm(FlaskForm):
    email = StringField('Email',validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')

class AnimalForm(FlaskForm):
    location = SelectField('Location', choices=[('Alipore','Alipore'),('Behala','Behala'),('Beliaghata','Beliaghata'),('Dum Dum','Dum Dum'),('Gariahat','Gariahat'), ('Hazra','Hazra'),('Jadavpur','Jadavpur'), ('Park Street','Park Street'),('Rajarhat','Rajarhat'),('Salt Lake','Salt Lake'),('Tollygunge','Tollygunge')])
    animal = SelectField('Animal Category', choices=[('Cat','Cat'), ('Dog','Dog'), ('Cow','Cow'),('Other','Other')])
    description = StringField('Description', validators=[DataRequired(), Length(min=1, max=512)])
    picture = FileField('Upload Animal Picture', validators=[DataRequired(),FileAllowed(['jpg', 'jpeg', 'png'])])
    bleeding = SelectField('Is Bleeding? ', choices=[('yes', 'yes'), ('no', 'no')])
    immediate = SelectField('Need Immediate assistance ?', choices=[('yes', 'yes'), ('no', 'no')])
    longterm = SelectField('Need Longterm assistance ?', choices=[('yes', 'yes'), ('no', 'no')])
    submit = SubmitField('Submit Abuse Report')

    '''def validate_duplicate_record(self, location, description):
        loc = Animal.query.filter_by(location=location.data).first()
        desc = Animal.query.filter_by(description=description.data).first()
        if loc and desc:
            raise ValidationError('This Abuse case is already listed. Most probably you are entering duplicate record!!')'''

class PledgePublicForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=1, max=512)])
    location = SelectField('Location', choices=[('Alipore','Alipore'),('Behala','Behala'),('Beliaghata','Beliaghata'),('Dum Dum','Dum Dum'),('Gariahat','Gariahat'), ('Hazra','Hazra'),('Jadavpur','Jadavpur'), ('Park Street','Park Street'),('Rajarhat','Rajarhat'),('Salt Lake','Salt Lake'),('Tollygunge','Tollygunge')])
    pick_up_date = SelectField('Pickup Date', choices=[('Monday','Monday'), ('Tuesday','Tuesday'), ('Wednesday','Wednesday'), ('Thursday','Thursday'), ('Friday','Friday'), ('Saturday','Saturday'), ('Sunday','Sunday')])
    pick_up_time = SelectField('Pickup Time', choices=[('Morning','Morning'), ('Noon','Noon'), ('Night','Night')])
    food_type = SelectField('Food Type', choices=[('Chicken','Chicken'), ('Mutton','Mutton'), ('Eggs','Eggs'), ('Milk','Milk'), ('Fish','Fish'), ('Rice','Rice'), ('Biscuits','Biscuits')])
    food_quantity_metric = SelectField('Food Quantity Metric', choices=[('Kg','Kg'), ('Litres','Litres'), ('Dozens','Dozens'), ('Pieces','Pieces')])
    food_quantity_value = FloatField('Food Quantity Value', validators=[InputRequired()])
    submit = SubmitField('Submit Food Details')

class SearchAnimalForm(FlaskForm):
    location = SelectField('Location', choices=[('Alipore','Alipore'),('Behala','Behala'),('Beliaghata','Beliaghata'),('Dum Dum','Dum Dum'),('Gariahat','Gariahat'), ('Hazra','Hazra'),('Jadavpur','Jadavpur'), ('Park Street','Park Street'),('Rajarhat','Rajarhat'),('Salt Lake','Salt Lake'),('Tollygunge','Tollygunge')])
    animal = SelectField('Animal Category', choices=[('All','All'), ('Cat','Cats'), ('Dog','Dogs'), ('Cow','Cows'),('Other','Others')])
    submit = SubmitField('Search Animal')

class DateForm(FlaskForm):
    start_date = DateField('Start Date', format='%Y-%m-%d')
    end_date = DateField('End Date', format='%Y-%m-%d')
    animal = SelectField('Animal Category', choices=[('All','All'), ('Cat','Cats'), ('Dog','Dogs'), ('Cow','Cows'),('Other','Others')])
    submit = SubmitField('Search Animals')


class PledgeResidentForm(FlaskForm):
    pick_up_date = SelectField('Pickup Date', choices=[('Monday','Monday'), ('Tuesday','Tuesday'), ('Wednesday','Wednesday'), ('Thursday','Thursday'), ('Friday','Friday'), ('Saturday','Saturday'), ('Sunday','Sunday')])
    pick_up_time = SelectField('Pickup Time', choices=[('Morning','Morning'), ('Noon','Noon'), ('Night','Night')])
    food_type = SelectField('Food Type', choices=[('Chicken','Chicken'), ('Mutton','Mutton'), ('Eggs','Eggs'), ('Milk','Milk'), ('Fish','Fish'), ('Rice','Rice'), ('Biscuits','Biscuits')])
    food_quantity_metric = SelectField('Food Quantity Metric', choices=[('Kg','Kg'), ('Litres','Litres'), ('Dozens','Dozens'), ('Pieces','Pieces')])
    food_quantity_value = FloatField('Food Quantity Value', validators=[InputRequired()])
    submit = SubmitField('Submit Food Details')

class ViewFoodForm(FlaskForm):
    pick_up_date = SelectField('Pickup Date', choices=[('Monday','Monday'), ('Tuesday','Tuesday'), ('Wednesday','Wednesday'), ('Thursday','Thursday'), ('Friday','Friday'), ('Saturday','Saturday'), ('Sunday','Sunday')])
    pick_up_time = SelectField('Pickup Time', choices=[('Morning','Morning'), ('Noon','Noon'), ('Night','Night')])
    submit = SubmitField('Submit Food Details')

