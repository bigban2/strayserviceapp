from datetime import datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'


app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///databases/strayservice.db'

db = SQLAlchemy(app)



class Login(db.Model):
    __tablename__='login'
    email = db.Column(db.String(30), nullable=False, primary_key=True)
    password = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(512), nullable=False)
    location = db.Column(db.String(512), nullable=False)
    usertype= db.Column(db.String(1), nullable=False)
    
    def __repr__(self):
        return f"Login('{self.name}', '{self.location}','{self.usertype}')"

class Animal(db.Model):
    __tablename__='animal'
    datetime = db.Column(db.DateTime,default=datetime.utcnow,nullable=False,primary_key=True)
    image_file = db.Column(db.String(20), nullable=False)
    location = db.Column(db.String(512), nullable=False)
    animal = db.Column(db.String(512), nullable=False)
    description = db.Column(db.String(512))
    bleeding = db.Column(db.String(4), nullable=False)
    immediate = db.Column(db.String(4), nullable=False)
    longterm = db.Column(db.String(4), nullable=False)
    def __repr__(self):
        return f"Animal('{self.datetime}', '{self.image_file}', '{self.location}', '{self.description}', '{self.bleeding}', '{self.immediate}', '{self.longterm}')"

class Donar(db.Model):
    __tablename__='donar'
    donar_id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    pick_up_date =  db.Column(db.String(10), nullable=False)
    pick_up_time =  db.Column(db.String(10), nullable=False)
    name = db.Column(db.String(512), nullable=False)
    location = db.Column(db.String(512), nullable=False)
    ref = db.relationship('Food', backref='donar', lazy=True,uselist=False)
    
    def __repr__(self):
        return f"Donar('{self.donar_id}', '{self.pick_up_date}','{self.pick_up_time}' , '{self.name}', '{self.location}')"

class Food(db.Model):
    __tablename__='food'
    donar_id = db.Column(db.Integer, db.ForeignKey('donar.donar_id'),primary_key=True,autoincrement=True)
    food_type = db.Column(db.String(20), nullable=False)
    food_quantity_value = db.Column(db.Integer, nullable=False)
    food_quantity_metric= db.Column(db.String(10), nullable=False) 
    
    def __repr__(self):
        return f"Food('{self.donar_id}', '{self.food_type}', '{self.food_quantity_value }','{self.food_quantity_metric}')"

